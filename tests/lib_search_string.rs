extern crate ogrep;

/// A basic test checking the core libraries validity by feeding a single liner
/// with the query inside ("hello" *is* inside of "hello there" so it should
/// have the proper ogrep::OGrepResult Vector with 1 item).
#[test]
fn basic_valid() {
    let query = "hello";
    let content = "hello there";

    assert_eq!(
        vec![ogrep::OGrepResult {
            line_num: 1,
            matching_text: content.to_string()
        }],
        ogrep::search_string(query, content)
    );
}

/// An attempt to see if it is capturing line-by-line (right) or each word
/// (wrong) by feeding it the same string repeating on 1 line. This should
/// ideally result in a length of just 1 inside of the Vector.
#[test]
fn multi_valid_lie() {
    let query = "hello";
    let content = "hello hello";

    assert_eq!(
        vec![ogrep::OGrepResult {
            line_num: 1,
            matching_text: content.to_string()
        }],
        ogrep::search_string(query, content)
    );
}
