/// OGrepResult is the main result structure from the OGrep library.
/// It includes the line number and the line itself that matches.
#[derive(Debug, PartialEq)]
pub struct OGrepResult {
    pub line_num: u32,
    pub matching_text: String,
}

/// search_string is the most basic ogrep query that is also the backend to the
/// more complex items. It parses lines. generates OGrepResult structures and
/// returns a vector of the mentioned structures.
///
/// # Arguments
///
/// - query (&str): The search query to use when searching
/// - content (&str): The content/body of text that ogrep::search_string() searches through to find any matches
///
/// # Examples
///
/// *Coming soon*
pub fn search_string(query: &str, content: &str) -> Vec<OGrepResult> {
    let mut ogrep_matches: Vec<OGrepResult> = vec![];

    for (line_ind, line) in content.lines().enumerate() {
        if line.contains(query) {
            ogrep_matches.push(OGrepResult {
                line_num: (line_ind as u32) + 1,
                matching_text: line.to_owned(),
            });
        }
    }

    ogrep_matches
}
