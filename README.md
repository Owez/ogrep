<div style="text-align:center;padding-top:1rem;">
    <img src="logo.png" width=128 style="border-radius:1rem">
</div>

## Overview

### About

OGrep is a very basic implamentation of the grep command, only searching text or files given by making use of the `structop` extention.

### Design Goals

- Test-driven development
- Seperate library and cli
- Proper `Result<>` return
- Use of `structop` for good-looking cli parsing
- Opening files with -f or simply searching from a string
- *FUTURE*: Cli piping
- *FUTURE*: Regex search 

## Installation

### Quickstart

1. Make sure you have Rust nightly installed
2. Open a terminal
3. `cargo install ogrep`
4. `ogrep [query] [text]` to search text, `ogrep [query] -f [filename]` to search a file
5. Finished! :tada:

## Building

1. Clone the repository with `git clone https://gitlab.com/scOwez/ogrep`
2. Make sure you have rust nightly installed for best compatibility
3. Build OGrep with the simple `cargo build --release` (release mode for faster speed, remove if you'd like)
4. Add to PATH/`/usr/bin`/etc if you wish
5. `ogrep [query] [text]` to search text, `ogrep [query] -f [filename]` to search a file
6. Finished! :tada:
